## Prerrequisitos

Para implementar el chatbot, usaremos Keras, que es una biblioteca de aprendizaje profundo, NLTK, que es un kit de herramientas de procesamiento del lenguaje natural, y algunas bibliotecas útiles. Ejecute el siguiente comando para asegurarse de que todas las bibliotecas estén instaladas:

```sh
$ pip install tensorflow keras pickle nltk 
```

Todos los chatbots se rigen por los conceptos de NLP (procesamiento del lenguaje natural). La PNL se compone de dos cosas:

* NLU (comprensión del lenguaje natural): la capacidad de las máquinas para comprender el lenguaje humano como el inglés.

* NLG (Generación de lenguaje natural): la capacidad de una máquina para generar texto similar a oraciones escritas por humanos.

## Ejecutando el Chatbot

Ahora tenemos dos archivos separados, uno es ```train_chatbot.py```, que usaremos primero para entrenar el modelo.